provider "aws" {
  region = "us-east-1"
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "> 3.5.0"
    }
  }
}

resource "aws_s3_bucket" "malcolm-terralock-example" {
  bucket = "malcolm-terralock-example"
  acl    = "private"
}